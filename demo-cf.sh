#! /usr/local/bin/bash
export DELAY=0.05

clear
source $HOME/workspace/demo-tools/demoscript/demoscript

#LOGIN to CF, using PWS
doit cf api https://api.run.pivotal.io
cf login -u aley@pivotal.io -p Makeithappen123$ -s development

doit cf app overview-broker
doit cf create-service-broker overview-broker admin admin  https://overview-broker-ossna.cfapps.io --space-scoped
doit open https://overview-broker-ossna.cfapps.io/dashboard
doit cf marketplace -s overview-broker
doit cf create-service overview-broker simple overview-instance
doit cf services
doit cf service overview-instance
doit open http://overview-broker-ossna.cfapps.io/dashboard
doit cd $HOME/workspace/cf-sample-app-spring
cd $HOME/workspace/cf-sample-app-spring
doit ls
doit cf app cf-spring

doit cf bind-service cf-spring overview-instance
doit open https://overview-broker-ossna.cfapps.io/dashboard

doit cf restart cf-spring
doit cf env cf-spring
doit cf unbind-service cf-spring overview-instance
doit cf delete-service -f overview-instance
doit open http://overview-broker-ossna.cfapps.io/dashboard
