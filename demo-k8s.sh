#! /usr/local/bin/bash
export DELAY=0.05

clear
source $HOME/workspace/demo-tools/demoscript/demoscript
#---
#DEMO k8s
#---
doit minikube dashboard

#SEE no broker has yet been registered
doit kubectl --context=service-catalog get brokers,serviceclasses,instances,bindings

#GOTO the overview example
doit cd $HOME/workspace/cf-k8s-broker-demo
cd $HOME/workspace/cf-k8s-broker-demo

#REGISTER a broker - we will use the earlier deployed regis
doit cat overview-broker.yaml
doit kubectl --context=service-catalog create -f overview-broker.yaml

#GET the service class for the overview-broker service
#This shows details of the service and plans available
doit kubectl --context=service-catalog get serviceclasses
doit kubectl --context=service-catalog get serviceclass overview-broker -o yaml

#CREATE a namespace for development
#All of our service instances and bindings will live here
doit kubectl create ns development

# #CREATE a service instance on the default plan
doit cat overview-instance.yaml
doit kubectl --context=service-catalog create -f overview-instance.yaml

#GET the service instance
doit kubectl --context=service-catalog -n development get instances
doit kubectl --context=service-catalog -n development get instances -o yaml
doit open http://overview-broker-ossna.cfapps.io/dashboard

#CREATE a binding to the instance
doit cat overview-binding.yaml
doit kubectl --context=service-catalog create -f overview-binding.yaml

#GET the service binding
doit kubectl --context=service-catalog -n development get binding
doit kubectl --context=service-catalog -n development get binding  -o yaml
doit open http://overview-broker-ossna.cfapps.io/dashboard

#GET the secret
doit kubectl get secrets -n development
doit kubectl get secrets -n development overview-credentials -o yaml

#DELETE the binding
doit kubectl --context=service-catalog -n development get binding
doit kubectl --context=service-catalog -n development delete binding overview-binding

#SEE binding and secret has gone
doit kubectl --context=service-catalog -n development get binding
doit kubectl get secrets -n development
doit open http://overview-broker-ossna.cfapps.io/dashboard
