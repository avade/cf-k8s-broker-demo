#!/bin/bash


cf delete-service-broker -f overview-broker
kubectl --context=service-catalog -n mucon-bytes delete instance overview-instance
kubectl --context=service-catalog delete broker overview-broker
kubectl delete ns mucon-bytes

#OPTIONAL instance cleanup
cf delete-service overview-instance
kubectl --context=service-catalog -n mucon-bytes delete instance overview-instance
kubectl --context=service-catalog -n mucon-bytes delete binding overview-binding

#OPTIONAL delete catalog
cd $HOME/workspace/service-catalog
helm delete --purge catalog
kubectl delete ns catalog
