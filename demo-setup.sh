#! /usr/local/bin/bash

cf api api.run.pivotal.io
cd $HOME/workspace/overview-broker
cf push

#SETUP kube
cd $HOME/workspace/service-catalog
minikube start
#DEPLOY the service-catalog
#Use helm to setup the components
helm init
helm install charts/catalog --name catalog --namespace catalog

#GET the address of the controller
SVC_CAT_API=$(minikube service -n catalog catalog-catalog-apiserver --url | sed -n 1p)
echo $SVC_CAT_API

#CREATE a new kubectl context
kubectl config set-cluster service-catalog --server=$SVC_CAT_API
kubectl config set-context service-catalog --cluster=service-catalog
